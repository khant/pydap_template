FROM tiangolo/uwsgi-nginx-flask:python3.9

#Copy all dependency files
COPY ./requirements.txt .
COPY ./app /app
COPY ./pydap.conf /etc/nginx/conf.d/

#Install all pacakges
RUN \
    apt-get update && \
    apt-get install -y curl python2 netcdf-bin libnetcdf-dev nano rsync && \
    pip install --upgrade pip && \
    #pip install --upgrade setuptools \
    pip install -r requirements.txt

    RUN rsync -r /usr/local/lib/python3.9/site-packages/pydap/wsgi/templates/static/ /app/static/

    #Install RClone
RUN curl https://rclone.org/install.sh | bash

#Copy custom files to PyDAP folders inside the container
COPY ./handlers/csv/__init__.py /usr/local/lib/python3.9/site-packages/pydap/handlers/csv/
COPY ./handlers/hdf5/__init__.py /usr/local/lib/python3.9/site-packages/pydap/handlers/hdf5/
COPY ./templates/base.html /usr/local/lib/python3.9/site-packages/pydap/wsgi/templates/
COPY ./templates/index.html /usr/local/lib/python3.9/site-packages/pydap/wsgi/templates/