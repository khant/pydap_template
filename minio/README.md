
## Accessing the Minio S3-API

#### Minio

- access to admin-frontend: https://minio.ufz.de:9000/
- login with UFZ-credentials
- to work with the scripts, create credentials for S3-API-access under 'Access Keys'
  - create Access- and Secretkey
  - (leave 'Restrict beyond user policy' as OFF)


#### Prerequisites

- copy *.s3-credentials.example* to *.s3-credentials* in this directory 
- (the file will be ignored by git)
- insert your credentials into the file

#### Usage

##### To upload all files in a folder recursively (and keeping the directory structure), e.g.:


    ./s3-upload.sh -d testdata


##### To upload single file, e.g.:


    ./s3-upload.sh -f testdata/hello-world.txt
